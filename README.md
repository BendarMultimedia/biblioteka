# Biblioteka

Zadanie testowe w ramach rekrutacji do multiprojekt.
Mało czasu miałem, więc na pewno brakuje wielu rzeczy.
Spis funkcji:
- Zarządzanie użytkownikami
- Zarządzanie książkami
- Wyszukiwarka pozycji
- Rezerwacja - użytkownik zarejestrowany
- Wypożyczenie - administrator
- Sprawdzanie dostępności adresu email przez AJAX

Jako zewnętrzne źródła użyte:
Font Awesome
Bootstrap
jQuery